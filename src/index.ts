import { Engine, Scene, HemisphericLight, Vector3, CannonJSPlugin } from "babylonjs";
import * as Cannon from "cannon";
import { MaterialSet } from "./materialSet";
import { Input } from './input';
import { LevelLayout } from './levelLayout';
import { Player } from './player';
import { CustomCamera } from './customCamera';

export class Main {
    constructor() {
    }

    public scene : Scene;
    public engine: Engine;
    
    private canvas       : HTMLCanvasElement;
    private physicsPlugin: CannonJSPlugin = new CannonJSPlugin(true, 10, Cannon);
    private isFlaggedForDisposal: boolean;

    public initialize(): void {
        this.canvas = <HTMLCanvasElement>document.getElementById("renderCanvas");

        this.loadEngine();
    }

    public loadEngine(): void {
        this.engine = new Engine(this.canvas, true);
        this.scene  = new Scene(this.engine);
        this.addPhysics();
        
        new HemisphericLight("Light", new Vector3(1, 1, 0), this.scene);
        new MaterialSet();
        new Input();
        new Player();
        new LevelLayout();
        new CustomCamera();
        
        this.engine.runRenderLoop(() => {
            this.update();
        });
    }

    private unloadEngine() {
        if (!this.isFlaggedForDisposal) { return; }
        
        // Do this at the end of the render loop or suffer an undefined camera bug.
        this.isFlaggedForDisposal = false;
        this.engine.dispose();
        this.loadEngine();
    }
    
    private addPhysics(): void {
        this.scene.enablePhysics(new Vector3(0, -9.81, 0), this.physicsPlugin);
        this.scene.gravity = new Vector3(0, -9.81, 0);
        this.scene.getPhysicsEngine();
    }

    private update(): void {
        this.scene.render();
        this.unloadEngine();
    }

    displayEndUI() {
        alert(
            `Game Over`
        );

        // Prepare our scene for disposal on the next render loop.
        this.isFlaggedForDisposal = true;
    }
}

export const MAIN: Main = new Main();
MAIN.initialize();