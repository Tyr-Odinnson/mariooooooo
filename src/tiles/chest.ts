import { MeshBuilder, PhysicsImpostor, Vector2 } from 'babylonjs';
import { MAIN } from '../index';
import { Tile } from './tile';
import { MaterialSet } from '../materialSet';
import { Player } from '../player';

export class Chest extends Tile {
    constructor() {
        super(
            MeshBuilder.CreateBox(`Chest_${++Chest.ID}`, {}, MAIN.scene)
        ,   MaterialSet.INSTANCE.matChest
        );
    }

    private static ID: number = -1;

    protected initialize() {
        super.initialize();

        this.mesh.physicsImpostor.registerOnPhysicsCollide(
            Player.INSTANCE.mesh.physicsImpostor, (colliderChest: PhysicsImpostor, colliderPlayer: PhysicsImpostor) => {
                var positionChest: Vector2 = colliderChest.physicsBody.position;
                var positionPlayer: Vector2 = colliderPlayer.physicsBody.position;

                if (positionPlayer.y < positionChest.y) {
                    var differenceX: number  = Math.abs(positionChest.x - positionPlayer.x);
                    if (differenceX < 1) {
                        console.log("score!");
                    }
                }
            }
        );
    }
}