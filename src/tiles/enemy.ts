import { MeshBuilder, PhysicsImpostor, Vector2, Ray, Vector3 } from 'babylonjs';
import { MAIN } from '../index';
import { Tile } from './tile';
import { MaterialSet } from '../materialSet';
import { Player } from '../player';

export class Enemy extends Tile {
    constructor() {
        super(
            MeshBuilder.CreateBox(`Enemy_${++Enemy.ID}`, {}, MAIN.scene)
        ,   MaterialSet.INSTANCE.matEnemy
        );

        // Custom code goes here.
    }

    private static ID: number = -1;

    private direction: number = 0;
    private speed: number = 7;

    protected initialize() {
        super.initialize();

        // Custom code goes here.
        // Check the Player for "registerBeforeRender" reference.
        // Check the chest for "collision" reference as well as detecting which side was hit.
        this.mesh.physicsImpostor.mass = 1;
        console.log(this.mesh.physicsImpostor.mass);

        this.mesh.physicsImpostor.registerOnPhysicsCollide(
            Player.INSTANCE.mesh.physicsImpostor, (colliderEnemy: PhysicsImpostor, colliderPlayer: PhysicsImpostor) => {
                var positionChest: Vector2 = colliderEnemy.physicsBody.position;
                var positionPlayer: Vector2 = colliderPlayer.physicsBody.position;

                if (positionPlayer.y < positionChest.y) {
                    var differenceX: number  = Math.abs(positionChest.x - positionPlayer.x);
                    if (differenceX < 1) {
                        console.log("score!");
                    }
                }
            }
        );
    }

    private update () {
        
        var velocityY = this.mesh.physicsImpostor.getLinearVelocity().y;
        this.mesh.physicsImpostor.setLinearVelocity(Vector3.Left().scale(this.speed)); 
    }
}