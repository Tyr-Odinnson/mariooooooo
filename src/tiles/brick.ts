import { MeshBuilder } from 'babylonjs';
import { MAIN } from '../index';
import { Tile } from './tile';
import { MaterialSet } from '../materialSet';

export class Brick extends Tile {
    constructor() {
        super(
            MeshBuilder.CreateBox(`Brick_${++Brick.ID}`, {}, MAIN.scene)
        ,   MaterialSet.INSTANCE.matBrick
        );

        // Custom code goes here.
    }

    private static ID: number = -1;

    protected initialize() {
        super.initialize();

        // Custom code goes here.
        // Check the Player for "registerBeforeRender" reference.
        // Check the chest for "collision" reference as well as detecting which side was hit.
    }
}