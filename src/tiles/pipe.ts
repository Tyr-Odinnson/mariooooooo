import { MeshBuilder } from 'babylonjs';
import { MAIN } from '../index';
import { Tile } from './tile';
import { MaterialSet } from '../materialSet';

export class Pipe extends Tile {
    constructor() {
        super(
            MeshBuilder.CreateBox(`Pipe_${++Pipe.ID}`, {}, MAIN.scene)
        ,   MaterialSet.INSTANCE.matPipe
        );
    }

    private static ID: number = -1;

    protected initialize() {
        super.initialize();
    }
}