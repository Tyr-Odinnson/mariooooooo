import { MeshBuilder } from 'babylonjs';
import { MAIN } from '../index';
import { Tile } from './tile';
import { MaterialSet } from '../materialSet';

export class Ground extends Tile {
    constructor() {
        super(
            MeshBuilder.CreateBox(`Ground_${++Ground.ID}`, {}, MAIN.scene)
        ,   MaterialSet.INSTANCE.matGround
        );
    }

    private static ID: number = -1;

    protected initialize() {
        super.initialize();
    }
}