import { Mesh, StandardMaterial, PhysicsImpostor } from 'babylonjs';
import { MAIN } from '..';

export class Tile {
    constructor(_mesh: Mesh, _material: StandardMaterial) {
        this.mesh = _mesh;
        this.mesh.material = _material;

        this.initialize();
    }
    
    public mesh: Mesh;
    
    protected initialize() {
        this.mesh.physicsImpostor = new PhysicsImpostor(
            this.mesh
        ,   PhysicsImpostor.BoxImpostor
        ,   { mass: 0, friction: 0, restitution: 0 }
        ,   MAIN.scene
        );
    }
}