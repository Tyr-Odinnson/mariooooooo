import { Mesh, MeshBuilder, Vector3, PhysicsImpostor, Scalar, Ray } from 'babylonjs'
import { MAIN } from './index'
import { MaterialSet } from './materialSet';
import { Input } from './input';
import { MathUtils } from './mathUtils';

export class Player {
    constructor() {
        this.initialize();

        Player.INSTANCE = this;
    }

    public static INSTANCE: Player;
    
    public mesh: Mesh;
    
    private direction: number = 0;
    private speed: number = 7;
    private jumpPower: number = 8;

    initialize() {
        this.mesh = MeshBuilder.CreateBox("Player", {}, MAIN.scene);
        this.mesh.material = MaterialSet.INSTANCE.matPlayer;
        this.mesh.position.y = 1;

        this.mesh.physicsImpostor = new PhysicsImpostor(this.mesh, PhysicsImpostor.BoxImpostor, { mass: 1, friction: 0, restitution: 0 }, MAIN.scene);
        this.mesh.isPickable = false;

        MAIN.scene.registerBeforeRender(() => {
            this.update();
        });
    }

    private update() {
        this.direction = Scalar.MoveTowards(this.direction, 0, (MAIN.engine.getDeltaTime() / 1000) * 2);
        
        var isGrounded: boolean = this.getIsGrounded();
        var velocityX = this.mesh.physicsImpostor.getLinearVelocity().x;

        // console.log(isGrounded);
        if (Input.IS_W_PRESSED && isGrounded) {
            // console.log("I should jump.");
            this.mesh.physicsImpostor.setLinearVelocity(new Vector3(velocityX, this.jumpPower, 0));
        }
        
        // 0.016666666666666666
        var increment = MAIN.engine.getDeltaTime() / 1000 * 3;
        if (Input.IS_A_PRESSED) {
            this.direction = MathUtils.CLAMP(this.direction - increment, -1, 1);
        }
        if (Input.IS_D_PRESSED) {
            this.direction = MathUtils.CLAMP(this.direction + increment, -1, 1);
        }
        
        var velocityY = this.mesh.physicsImpostor.getLinearVelocity().y;
        this.mesh.physicsImpostor.setLinearVelocity(
            new Vector3(
                this.direction * this.speed
            ,   velocityY >= 0 ? velocityY : velocityY - .3
            ,   0
            )
        );
        this.restrictMotion();
    }

    private restrictMotion() {
        this.mesh.physicsImpostor.setAngularVelocity(Vector3.Zero());
        this.mesh.rotation = Vector3.Zero();
        this.mesh.position.z = 0;
    }

    getIsGrounded(): boolean {
        var ray = new Ray(this.mesh.position, Vector3.Down(), .51);
        var hit = MAIN.scene.pickWithRay(ray);
        
        if (hit.pickedMesh != null && hit.pickedMesh.name.search(/Enemy/) == -1) {
            return true;
        }

        return false;
    }
}