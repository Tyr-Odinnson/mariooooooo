import { MAIN } from './index';
import { ActionManager, ExecuteCodeAction } from 'babylonjs';

export class Input {
    constructor() {
        this.initialize();
    }

    public static IS_W_PRESSED = false;
    public static IS_A_PRESSED = false;
    public static IS_D_PRESSED = false;

    private initialize(): void {
        var map: any = {}; //object for multiple key presses
        MAIN.scene.actionManager = new ActionManager(MAIN.scene);

        MAIN.scene.actionManager.registerAction(
            new ExecuteCodeAction(
                ActionManager.OnKeyDownTrigger, (e) => {
                    map[e.sourceEvent.key] = e.sourceEvent.type == "keydown";
                }
            )
        );

        MAIN.scene.actionManager.registerAction(
            new ExecuteCodeAction(
                ActionManager.OnKeyUpTrigger, (e) => {
                    map[e.sourceEvent.key] = e.sourceEvent.type == "keydown";
                }
            )
        );

        MAIN.scene.registerAfterRender(() => {
            this.getKeys(map);
        });
    }

    private getKeys(map: any) {
        Input.IS_W_PRESSED = (map["w"] || map["W"]) ? true : false;
        Input.IS_W_PRESSED = (map["w"] || map["W"]) ? true : false;
        Input.IS_A_PRESSED = (map["a"] || map["A"]) ? true : false;
        Input.IS_D_PRESSED = (map["d"] || map["D"]) ? true : false;
    }
}