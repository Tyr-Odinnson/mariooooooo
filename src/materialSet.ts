import { StandardMaterial, Color3 } from 'babylonjs';
import { MAIN } from '.';

export class MaterialSet {
    constructor() {
        this.initialize();
        MaterialSet.INSTANCE = this;
    }

    public static INSTANCE: MaterialSet;;
    
    public matPlayer: StandardMaterial;
    public matGround: StandardMaterial;
    public matStep  : StandardMaterial;
    public matBrick : StandardMaterial;
    public matChest : StandardMaterial;
    public matPipe  : StandardMaterial;
    public matEnemy : StandardMaterial;
    
    initialize(): void {
        this.matPlayer = new StandardMaterial("matPlayer", MAIN.scene);
        this.matPlayer.diffuseColor = Color3.Purple();

        this.matGround = new StandardMaterial("matGround", MAIN.scene);
        this.matGround.diffuseColor = new Color3(.3, .2, .0);
        
        this.matStep = new StandardMaterial("matStep", MAIN.scene);
        this.matStep.diffuseColor = Color3.Magenta();

        this.matPipe = new StandardMaterial("matPipe", MAIN.scene);
        this.matPipe.diffuseColor = Color3.Green();

        this.matBrick = new StandardMaterial("matBrick", MAIN.scene);
        this.matBrick.diffuseColor = Color3.Blue();

        this.matChest = new StandardMaterial("matChest", MAIN.scene);
        this.matChest.diffuseColor = Color3.Yellow();

        this.matEnemy = new StandardMaterial("matEnemy", MAIN.scene);
        this.matEnemy.diffuseColor = Color3.Red();
    }
}