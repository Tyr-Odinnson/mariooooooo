import { MAIN } from ".";
import { UniversalCamera, Vector3, Camera, MeshBuilder, Mesh, PhysicsImpostor } from "babylonjs";
import { Player } from "./player";

export class CustomCamera {
    constructor() {
        CustomCamera.INSTANCE = this;

        this.initialize();
        
        MAIN.scene.registerBeforeRender(() => {
            this.move();
        });
    }

    public static INSTANCE: CustomCamera;
    
    public camera: UniversalCamera;

    initialize() {
        this.createCamera();
        this.createBarriers();
    }

    private createCamera() {
        this.camera = new UniversalCamera("Camera", new Vector3(6.5, 3.5, -10), MAIN.scene);

        this.camera.mode = Camera.ORTHOGRAPHIC_CAMERA;

        var orthographicScaleX = 8;
        var orthographicScaleY = 6;
        this.camera.orthoTop = orthographicScaleY;
        this.camera.orthoBottom = -orthographicScaleY;
        this.camera.orthoLeft = -orthographicScaleX;
        this.camera.orthoRight = orthographicScaleX;
    }

    createBarriers() {
        var side: Mesh = MeshBuilder.CreateBox('BarrierSide', {height:12}, MAIN.scene);
        side.position.x -= 2;
        side.position.y = 3.5;
        side.physicsImpostor = new PhysicsImpostor(side, PhysicsImpostor.BoxImpostor, {mass:0, friction:0, restitution:0}, MAIN.scene);
        side.setParent(this.camera);

        var top: Mesh = MeshBuilder.CreateBox('BarrierSide', {width:16}, MAIN.scene);
        top.position.x += 6.5;
        top.position.y = 10;
        top.physicsImpostor = new PhysicsImpostor(top, PhysicsImpostor.BoxImpostor, {mass:0, friction:0, restitution:0}, MAIN.scene);
        top.setParent(this.camera);
    }

    move() {
        var cameraOffset = 2;
        var playerPos: Vector3 = Player.INSTANCE.mesh.position; 
        var playerOffset = playerPos.x - cameraOffset;
        
        if (playerOffset > this.camera.position.x) {
            this.camera.position.x = playerOffset;
        }
    }
}