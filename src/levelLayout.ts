import { Vector2 } from 'babylonjs';
import { Tile } from './tiles/tile';
import { Chest } from './tiles/chest';
import { Brick } from './tiles/brick';
import { Pipe } from './tiles/pipe';
import { Ground } from './tiles/ground';
import { Enemy} from './tiles/enemy';

enum TileType {
    Ground
,   Chest
,   Brick
,   Pipe
,   Enemy
}

export class LevelLayout {
    constructor() {
        this.initialize();
    }
    
    initialize() {
        this.drawTiles([-1, -1], [18, 2], TileType.Ground);
        
        this.drawTiles([2, 3], [1, 1], TileType.Chest);

        this.drawTiles([ 6, 3], [1, 1], TileType.Brick);
        this.drawTiles([ 7, 3], [1, 1], TileType.Chest);
        this.drawTiles([ 8, 3], [1, 1], TileType.Brick);
        this.drawTiles([ 9, 3], [1, 1], TileType.Chest);
        this.drawTiles([10, 3], [1, 1], TileType.Brick);

        this.drawTiles([6, 2], [1, 1], TileType.Enemy);

        this.drawTiles([8, 7], [1, 1], TileType.Chest);

        this.drawTiles([14, 1], [2, 2], TileType.Pipe);
    }

    drawTiles(_position: number[], _scale: number[], _type: TileType) {
        var position: Vector2 = new Vector2(_position[0], _position[1]);
        var scale: Vector2 = new Vector2(_scale[0], _scale[1]);

        for (let i = 0; i < scale.y; i++) {
            for (let j = 0; j < scale.x; j++) {
                var tile: Tile = this.getTileType(_type);
                tile.mesh.position.y = position.y - i;
                tile.mesh.position.x = position.x + j;
            }
        }
    }

    getTileType(_type: TileType): Tile {
        switch (_type) {
            case TileType.Ground:
                return new Ground();
            case TileType.Brick:
                return new Brick();
            case TileType.Chest:
                return new Chest();
            case TileType.Pipe:
                return new Pipe();
            case TileType.Enemy:
                return new Enemy();
            default:
                return new Ground();
        }
    }
}