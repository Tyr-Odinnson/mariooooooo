import { Vector3, Vector2 } from "babylonjs";

export class MathUtils {
    constructor() {
    }

    static DEG_TO_RAD(_angle: number): number{
        return _angle * (Math.PI / 180.0);
    }

    static RAD_TO_DEG(_angle: number): number {
        return (_angle / Math.PI) * 180.0;
    }

    static CLAMP(_value: number, _min: number, _max: number): number {
        if (_value < _min) {
            return _min;
        }

        if (_value > _max) {
            return _max;
        }

        return _value;
    }

    static LERP(_a: number, _b: number, _t: number): number {
        return (1 - _t) * _a + _t * _b;
    }

    static INVERSE_LERP(_a: number, _b: number, _t: number): number {
        return (_t - _a) / (_b - _a);
    }

    static DISTANCE(_a: Vector3, _b: Vector3): number {
        return Vector3.Distance(_a, _b);
    }

    static NTH_ROOT(_value: number, n: number): number {
        return Math.pow(_value, 1/n);
    }

    static REMAP(_value: number, _inMinMax: Vector2, _ouMinMax: Vector2): number {
        return _ouMinMax.x + (_value - _inMinMax.x) * (_ouMinMax.y - _ouMinMax.x) / (_inMinMax.y - _inMinMax.x);
    }
}