# Knucklebones

In this game you throw a ball in the air using AmmoJS and try to click & collect all the surrounding jacks before the ball hits the ground again.

You need to install AmmoJS for this to work.

See the following pages for help piecing this puzzle together.

### Official Documentation
- [BabylonJS - ES6 Support](https://doc.babylonjs.com/features/es6_support)
- [Use Forces](https://doc.babylonjs.com/how_to/forces)
- [Using the Physics Engine](https://doc.babylonjs.com/how_to/using_the_physics_engine)
- [Basic Physics Example](https://www.babylonjs-playground.com/#YUNAST#3)

### Forums
- [Including Ammo.js with BABYLON using Nodejs](https://forum.babylonjs.com/t/including-ammo-js-with-babylon-using-nodejs/5054)